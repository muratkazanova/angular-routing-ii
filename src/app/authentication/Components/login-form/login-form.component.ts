import { Component, OnInit } from '@angular/core';
import { UserType } from 'src/app/models/user';
import { LoginService } from '../../Services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  constructor(private loginService: LoginService) {}

  ngOnInit() {}

  onLogin(type: UserType) {
    switch (type) {
      case UserType.Admin: {
        this.loginService.LoginAsAdmin();
        break;
      }
      default: {
        this.loginService.LoginAsSiteUser();
      }
    }
  }
}
