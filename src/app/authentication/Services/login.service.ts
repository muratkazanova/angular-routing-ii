import { Injectable } from '@angular/core';
import { UserType, User } from 'src/app/models/user';
import { LoggerService } from 'src/app/logger/Services/logger.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  LoggedInUser: User;
  constructor(private loggerService: LoggerService, private router: Router) {}

  isLoggedOn(): boolean {
    return this.LoggedInUser ? true : false;
  }

  Login(user: User) {
    this.LoggedInUser = user;
    this.loggerService.log(`Logged in as ${user.UserName}`);
    this.router.navigate(['/employees']);
  }
  LoginAsAdmin() {
    this.Login({
      UserName: 'admin',
      Type: UserType.Admin
    });
  }

  LoginAsSiteUser() {
    this.Login({
      UserName: 'lcpsuser',
      Type: UserType.SiteUser
    });
  }
}
