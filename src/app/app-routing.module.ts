import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginFormComponent } from './authentication';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CustomPreloadingService as CustomPreloadingStrategy } from './Services/custom-preloading.service';
import { LogMessagesComponent } from './logger';
import { EmployeeCanActivateGuard } from './Employee/Services/employee-can-activate-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginFormComponent },
  {
    path: 'employees',
    data: { preload: false },
    canActivate: [EmployeeCanActivateGuard],
    loadChildren: './Employee/employee.module#EmployeeModule'
  },
  {
    path: 'logs/:id',
    component: LogMessagesComponent,
    outlet: 'logListWidget'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: CustomPreloadingStrategy,
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
