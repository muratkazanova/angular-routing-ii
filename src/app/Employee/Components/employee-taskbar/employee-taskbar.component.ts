import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from '../../Services/employee.service';
import { IEmployee } from 'src/app/models/employee';
import { switchMap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-taskbar',
  templateUrl: './employee-taskbar.component.html',
  styleUrls: ['./employee-taskbar.component.scss']
})
export class EmployeeTaskbarComponent implements OnInit {
  @Input() employees: IEmployee[];
  detailsStyle: string;
  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.detailsStyle = params.has('detailStyle')
        ? params.get('detailStyle').toString()
        : 'inline';
    });
  }
  onNewEmployee() {
    this.employeeService.EmployeeFormEvent.emit(true);
  }

  onDetailsStyleChange(event: any) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { detailStyle: event.target.value }
    });
  }
}
