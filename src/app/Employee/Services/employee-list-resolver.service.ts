import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { IEmployee } from 'src/app/models/employee';
import { Observable, of } from 'rxjs';
import { EmployeeService } from './employee.service';
import { switchMap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class EmployeeListResolver implements Resolve<IEmployee[]> {
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): IEmployee[] | Observable<IEmployee[]> | Promise<IEmployee[]> {
    return this.employeeService.getDelayedEmployees();
  }

  constructor(private employeeService: EmployeeService) {}
}
