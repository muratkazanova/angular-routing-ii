import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from 'src/app/authentication/Services/login.service';
import { LoggerService } from 'src/app/logger/Services/logger.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeCanActivateGuard implements CanActivate {
  constructor(
    private loginService: LoginService,
    private loggerService: LoggerService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const result = this.loginService.isLoggedOn();
    if (result) {
      this.loggerService.log(
        'User is granted to navigate to Employee Dashboard'
      );
    } else {
      this.loggerService.log(
        'Anonymous user is not allowed to navigate to Employee pages.'
      );
    }

    return result;
  }
}
