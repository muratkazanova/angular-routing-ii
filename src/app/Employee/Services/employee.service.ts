import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, of, BehaviorSubject, empty, Subject } from 'rxjs';
import { IEmployee } from 'src/app/models/employee';
import { delay } from 'rxjs/operators';
import { LoggerService } from 'src/app/logger/Services/logger.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees: IEmployee[];
  // EmployeeSourceChanges = new BehaviorSubject('initial');
  EmployeeSourceChanges = new Subject();
  @Output()
  EmployeeFormEvent: EventEmitter<boolean> = new EventEmitter();
  constructor(private loggerService: LoggerService) {
    this.employees = this.employees = [
      {
        id: 1,
        FirstName: 'Ken',
        LastName: 'Sanch',
        Gender: 'M',
        EmailAddress: 'ken0@adventure-works.com',
        PhoneNumber: '697-555-0142'
      },
      {
        id: 2,
        FirstName: 'Terri',
        LastName: 'Duffy',
        Gender: 'F',
        EmailAddress: 'terri0@adventure-works.com',
        PhoneNumber: '819-555-0175'
      },
      {
        id: 3,
        FirstName: 'Roberto',
        LastName: 'Tamburell',
        Gender: 'M',
        EmailAddress: 'roberto0@adventure-works.com',
        PhoneNumber: '212-555-0187'
      }
    ];
  }

  getEmployee(id: number): Observable<IEmployee> {
    this.loggerService.log(`Getting employee by employee ID: ${id}.`);
    return of(this.employees.find(e => e.id === id));
  }

  getDelayedEmployees(): Observable<IEmployee[]> {
    this.loggerService.log('Getting employees..');
    return of(this.employees).pipe(delay(2000));
  }
  getEmployees(): Observable<IEmployee[]> {
    return of(this.employees);
  }

  newEmployee(value: { FirstName: string; LastName: string }) {
    const ids = this.employees.map(o => o['id']).sort();

    this.employees = [
      ...this.employees,
      {
        id: ids.length ? ids[ids.length - 1] + 1 : 1,
        FirstName: value.FirstName,
        LastName: value.LastName
      }
    ];
    this.EmployeeFormEvent.emit(false);
    this.EmployeeSourceChanges.next('New Employee');
    this.loggerService.log('New employee saved.');
  }

  updateEmployee(FirstName, LastName, Id) {
    const employee = this.employees.find(e => e.id === Id);
    employee.FirstName = FirstName;
    employee.LastName = LastName;
    this.EmployeeSourceChanges.next('Employee Updated');
    this.loggerService.log('Employee updated.');
    return of(null);
  }

  deleteEmployee(id: number) {
    this.employees = this.employees.filter(e => e.id != id);
    this.EmployeeSourceChanges.next('Employee Deleted');
    this.loggerService.log(`Employee # ${id} deleted.`);
    return of(null);
  }
}
