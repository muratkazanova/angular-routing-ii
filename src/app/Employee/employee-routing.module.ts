import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeDashboardComponent } from './Containers';
import { EmployeeDetailsComponent } from './Components';
import { EmployeeListResolver } from './Services/employee-list-resolver.service';
import { EmployeeCanDeactivateGuard } from './Services/employee-can-deactivate-guard.service';

const routes: Routes = [
  // {
  //   path: 'employees',
  //   component: EmployeeDashboardComponent,
  //   resolve: { employees: EmployeeListResolver },
  //   runGuardsAndResolvers: 'paramsChange'
  // },
  // { path: 'employees/:id', component: EmployeeDetailsComponent }
  {
    path: '',
    component: EmployeeDashboardComponent,
    resolve: { employees: EmployeeListResolver },
    runGuardsAndResolvers: 'paramsChange'
  },
  {
    path: ':id',
    component: EmployeeDetailsComponent,
    canDeactivate: [EmployeeCanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {}
