export enum UserType {
  Admin,
  SiteUser
}
export interface User {
  UserName: string;
  Type: UserType;
}
