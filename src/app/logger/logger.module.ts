import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogMessagesComponent } from './index';

@NgModule({
  declarations: [LogMessagesComponent],
  imports: [CommonModule]
})
export class LoggerModule {}
