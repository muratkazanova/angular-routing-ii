import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoggerService } from '../../Services/logger.service';

@Component({
  selector: 'app-log-messages',
  templateUrl: './log-messages.component.html',
  styleUrls: ['./log-messages.component.scss']
})
export class LogMessagesComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private loggerService: LoggerService
  ) {}

  id: string;
  logMessages: string[] = [];
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.logMessages = this.loggerService.messages;
  }

  toggleLogView() {
    this.loggerService.toggleLogView();
  }
}
