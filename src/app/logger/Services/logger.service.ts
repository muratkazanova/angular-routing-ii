import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  messages: string[] = [];
  isLogVisible = false;
  constructor(private router: Router) {}

  log(message: string) {
    this.messages.unshift(message);
  }

  toggleLogView() {
    this.isLogVisible = !this.isLogVisible;
    if (this.isLogVisible) {
      this.router.navigate([{ outlets: { logListWidget: ['logs', '74MK'] } }]);
    } else {
      this.router.navigate([{ outlets: { logListWidget: null } }]);
    }
  }
}
