import {
  Component,
  AfterViewInit,
  ViewChild,
  OnInit,
  ElementRef,
  Renderer2
} from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationError,
  NavigationCancel
} from '@angular/router';
import { LoggerService } from './logger/Services/logger.service';
import { LoginService } from './authentication/Services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'lcps';
  loading = false;
  constructor(
    private router: Router,
    private loggerService: LoggerService,
    public loginService: LoginService
  ) {}
  ngOnInit() {
    this.router.events.subscribe(routerEvent => {
      if (routerEvent instanceof NavigationStart) {
        this.loading = true;
      }

      if (
        routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationError ||
        routerEvent instanceof NavigationCancel
      ) {
        if (routerEvent instanceof NavigationEnd) {
          this.loggerService.log(
            `Routed path: ${JSON.stringify(routerEvent.url)}`
          );
        }

        this.loading = false;
      }
    });
  }

  onLogOut() {
    this.loginService.LoggedInUser = null;
    this.loggerService.log('User logged out.');
    this.router.navigate(['/home']);
  }

  toggleLogListWidget() {
    this.loggerService.toggleLogView();
  }
}
